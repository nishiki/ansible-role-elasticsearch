import os
import testinfra.utils.ansible_runner

def test_packages(host):
    package = host.package('elasticsearch')
    assert package.is_installed

def test_config_file(host):
    config = host.file('/etc/elasticsearch/elasticsearch.yml')
    assert config.user == 'root'
    assert config.group == 'elasticsearch'
    assert config.mode == 0o640
    assert config.contains('path.data: /var/lib/elasticsearch')

def test_ssl_key_file(host):
    config = host.file('/etc/elasticsearch/key.pem')
    assert config.user == 'root'
    assert config.group == 'elasticsearch'
    assert config.mode == 0o640
    assert config.contains('-----BEGIN PRIVATE KEY-----')

def test_ssl_certificate_file(host):
    config = host.file('/etc/elasticsearch/certificate.pem')
    assert config.user == 'root'
    assert config.group == 'elasticsearch'
    assert config.mode == 0o640
    assert config.contains('-----BEGIN CERTIFICATE-----')

def test_service(host):
    service = host.service('elasticsearch')
    assert service.is_running
    assert service.is_enabled

def test_socket(host):
    for port in [9200, 9300]:
        socket = host.socket('tcp://127.0.0.1:%d' % (port))
        assert socket.is_listening

def test_java_memory(host):
    process = host.process.filter(user='elasticsearch', comm='java')
    assert '-Xms512m' in process[1].args
    assert '-Xmx512m' in process[1].args

def test_elasticsearch_template(host):
    result = host.check_output('curl -v -u elastic:mysecret http://127.0.0.1:9200/_template/test')
    assert '"number_of_replicas":"1"' in result

def test_elasticsearch_role(host):
    result = host.check_output('curl -v -u elastic:mysecret http://127.0.0.1:9200/_security/role/myrole')
    assert '"names":["logstash*"]' in result
    assert '"privileges":["create","write"]' in result

def test_elasticsearch_user(host):
    result = host.check_output('curl -v -u elastic:mysecret http://127.0.0.1:9200/_security/user/toto')
    assert '"username":"toto"' in result
    assert '"roles":["viewer"]' in result
