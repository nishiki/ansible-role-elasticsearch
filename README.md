# Ansible role: Elasticsearch

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-elasticsearch/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-elasticsearch/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-elasticsearch/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-elasticsearch/actions?workflow=molecule.yml)

Install and configure Elasticsearch

## Requirements

- Ansible >= 2.9
- Debian
  - Bookworm

## Role variables

- `elasticsearch_major_version` - set the major version (default: `7`)
- `elasticsearch_master` - set if the node is master (default: `true`)
- `elasticsearch_heap_size` - set the heap size (default: `1g`)
- `elasticsearch_api_user` - set the admin user (default: `elastic`)
- `elasticsearch_api_password` - set the password for api
- `elasticsearch_config` - hash with the configuration (see [elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html))

```yaml
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
```

- `elasticsearch_ssl_key`: - string contain ssl private key if `xpack.security.transport.ssl.key` is defined in elasticsearch_config
- `elasticsearch_ssl_certificate`: - string contain ssl certificate if `xpack.security.transport.certificate.key` is defined in elasticsearch_config
- `elasticsearch_roles` - hash with the roles to managed

```yaml
myrole:
  cluster:
    - all
  indices:
    - names: ["logstash*"]
      privileges:
        - create
        - write
```

- `elasticsearch_users` - hash with the users to managed

```yaml
toto:
  password: supers3cret
  roles:
    - viewer
kibana_system:
  password: supertest2
```

- `elasticsearch_index_templates` - hash with the index templates configuration

```yaml
logstash:
  index_patterns:
    - "logstash-*"
  settings:
    index:
      number_of_replicas: 3
  mappings:
    metric:
      type: short
    date:
      type: date
      format: YYYY-MM-dd
```

- `elasticsearch_ilm_policies` - hash with the ilm policies configuration

```yaml
autoclean:
  delete:
    min_age: 30d
    actions:
      delete: {}
```

## How to use

```
- hosts: server
  roles:
    - elasticsearch
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule 'molecule[docker]' ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
