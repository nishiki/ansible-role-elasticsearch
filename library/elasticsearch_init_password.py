#!/usr/bin/python

from ansible.module_utils.basic import *
from ansible.module_utils.elasticsearch_api import *
import subprocess

class ElasticsearchInitPassword:
    def __init__(self, api_url, api_user, api_password):
        self.api = ElasticsearchApi(
            api_url,
            api_user,
            api_password
        )
        self.user = api_user
        self.password = api_password

    def is_set(self):
        status_code, _ = self.api.get('_cluster/health')
        if status_code == 401:
            return False

        return True

    def change(self):
        subprocess.run(
            ['/usr/share/elasticsearch/bin/elasticsearch-reset-password', '-u', self.user, '-b', '-i'],
            input='{}\n{}'.format(self.password, self.password).encode()
        )
        
def main():
    fields = {
        'api_url':      { 'type': 'str', 'default': 'http://127.0.0.1:9200' },
        'api_user':     { 'type': 'str', 'default': None },
        'api_password': { 'type': 'str', 'default': None, 'no_log': True },
    }
    module = AnsibleModule(argument_spec=fields)
    changed = False

    init = ElasticsearchInitPassword(
        module.params['api_url'],
        module.params['api_user'],
        module.params['api_password'],
    )

    if not init.is_set():
        init.change()
        changed = True

    module.exit_json(changed=changed)

if __name__ == '__main__':
    main()
