# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- feat: add variable to set master
- feat: manage user
- feat: manage role
- feat: add variable to set major version
- feat: add ilm policy
- test: add support debian 12

### Removed

- test: remove support debian 10
- test: remove support debian 11

### Changed

- major default version is 8
- replace kitchen to molecule
- replace apt_key to get_url
- test: use personal docker registry

### Fixed

- no_log only api_password for elasticsearch_template module
- add full python3 support

## v1.1.0 - 2019-11-21

### Added

- new option to set heap size
- manage index templates

## v1.0.0 - 2019-09-05

### Added

- install elasticsearch package
- copy configuration
